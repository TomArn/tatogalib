import toga


class NewWidgetImpl:
    def __init__(self, interface):
        self.interface = interface
        self.app = toga.App.app


version = "1.0.0"
version_date = "2024-02-20 - 2024-03-20"
