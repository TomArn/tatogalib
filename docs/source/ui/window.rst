window module
=============
.. toctree::
   :maxdepth: 2
   
.. automodule:: tatogalib.ui.window
   :members: centerOnParent
.. autofunction:: centerOnParent
.. autoattribute:: tatogalib.ui.window.version
.. autoattribute:: tatogalib.ui.window.version_date


TaGui class
-----------
.. autoclass:: tatogalib.ui.window.TaGui
   :members:


TaWindow class
--------------
.. autoclass:: tatogalib.ui.window.TaWindow
   :members:


HtmlWindow class
----------------
.. autoclass:: tatogalib.ui.window.HtmlWindow
   :members:


