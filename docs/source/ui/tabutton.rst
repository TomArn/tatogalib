tabutton package
================

This package is a plug-in replacement for toga.Button which allows
to use Icons with a user-defined size. The default size is
48x48 for Android and 32x32 for Winforms (CSS pixels).

To specify a different size, create a TaButton with text=None 
and then use the TaButton.set_icon(icon, size) method.

.. toctree::
   :maxdepth: 2
   
.. automodule:: tatogalib.ui.tabutton
   :members:

TaButton class
--------------
.. autoclass:: tatogalib.ui.tabutton.TaButton
   :members:
