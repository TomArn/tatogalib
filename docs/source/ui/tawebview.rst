tawebview package
=================

This package is a plug-in replacement for toga.WebView which allows 
to monitor and optionally block the URLs being navigated to. The package
is currently supported for Windows and Android.

.. toctree::
   :maxdepth: 2
   
.. automodule:: tatogalib.ui.tawebview
   :members:

TaWebView class
---------------
.. autoclass:: tatogalib.ui.tawebview.TaWebView
   :members:
