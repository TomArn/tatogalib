taselection package
===================

This package is a plug-in replacement for toga.Selection which implements the missing 
set_font() for Android.

.. toctree::
   :maxdepth: 2
   
.. automodule:: tatogalib.ui.taselection
   :members:

TaSelection class
-----------------
.. autoclass:: tatogalib.ui.taselection.TaSelection
   :members:
