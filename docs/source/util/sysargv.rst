sysargv module
==============
.. toctree::
   :maxdepth: 2
   
.. automodule:: tatogalib.util.sysargv

SysArgv class
-------------
.. autoclass:: tatogalib.util.sysargv.SysArgv
   :members:
