i18nUtils module
================
.. toctree::
   :maxdepth: 2
   
.. automodule:: tatogalib.util.i18nUtils
   :members:
.. autoattribute:: tatogalib.util.i18nUtils.version
.. autoattribute:: tatogalib.util.i18nUtils.version_date

I18nUtils class
---------------
.. autoclass:: tatogalib.util.i18nUtils.I18nUtils
   :members:
