appconfig module
================
.. toctree::
   :maxdepth: 2
   
.. automodule:: tatogalib.util.appconfig
   :members:
.. autoattribute:: tatogalib.util.appconfig.version
.. autoattribute:: tatogalib.util.appconfig.version_date
