uriinputstream package
======================

This package allows to read files in a cross-platform way.
It has been tested on Android and Windows, but should also work on Linux and macOS.
It is used by ``UriFile.open()`` when ``mode`` is "rb" or "rt"


.. toctree::
   :maxdepth: 2
   
.. automodule:: tatogalib.uri_io.uriinputstream
   :members:

UriInputStream class
--------------------
.. autoclass:: tatogalib.uri_io.uriinputstream.UriInputStream
   :members:

UriTextInputStream class
------------------------
.. autoclass:: tatogalib.uri_io.uriinputstream.UriTextInputStream
   :members:
