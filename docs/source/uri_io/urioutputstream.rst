urioutputstream package
=======================

This package allows to read files in a cross-platform way.
It has been tested on Android and Windows, but should also work on Linux and macOS.
It is used by ``UriFile.open()`` when ``mode`` is "wb" or "wt"


.. toctree::
   :maxdepth: 2
   
.. automodule:: tatogalib.uri_io.urioutputstream
   :members:

UriOutputStream class
---------------------
.. autoclass:: tatogalib.uri_io.urioutputstream.UriOutputStream
   :members:

UriTextOutputStream class
-------------------------
.. autoclass:: tatogalib.uri_io.urioutputstream.UriTextOutputStream
   :members:
