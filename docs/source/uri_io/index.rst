tatogalib.uri_io
================

uri_io is a cross-platform package for dealing with files and folders.
Files and folders are identified by URI-strings. On Android, they are content URI-strings,
e.g. ``"content://com.android.externalstorage.documents/document/primary%3ADownload%2FTest-dir%2Ftest.pdf"``
On desktops, they are file URI-strings, e.g. ``"file://C:/Program%20Files/test.pdf"``

.. toctree::
   :maxdepth: 2

   urifile
   urifilebrowser
   uriinputstream
   urioutputstream

   