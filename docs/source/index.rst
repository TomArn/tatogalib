.. taTogaLib documentation master file, created by
   sphinx-quickstart on Mon May  1 14:03:08 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to taTogaLib's documentation!
=====================================

Copyright: |copyright|

License: |license|

Release: |release|

Source code: |repository|

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. toctree::
   :maxdepth: 3
   :caption: Content:
   
   system/index
   ui/index
   uri_io/index
   util/index
   