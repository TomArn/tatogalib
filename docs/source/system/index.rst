tatogalib.system
================

.. toctree::
   :maxdepth: 2

   clipboard
   notifications

.. automodule:: tatogalib.system
.. autofunction:: get_platform
.. autofunction:: get_file_roots
  